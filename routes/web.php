<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AccueilController;
use App\Http\Controllers\AjoutRessourceController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\RessourceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[AccueilController::class,'PageAccueil'])->name('accueil');
Route::get('/ajouter-une-ressource',[AjoutRessourceController::class,'PageAjoutdeRessource'])->name('ajoutressource')->middleware('auth');
Route::post('/ajouter-une-ressource',[AjoutRessourceController::class,'FormRessource'])->name('ajoutressource')->middleware('auth');
Route::get('/ressource/{id}',[RessourceController::class,'PageRessource'])->name('ressource');
Route::get('/ressource/{id}/delete',[RessourceController::class,'DeleteRessource'])->name('deleteressource');
Route::post('/ressource/{id}/update',[RessourceController::class,'UpdateRessource'])->name('updateressource');
Route::get('/ressource/{id}/update',[RessourceController::class,'EditRessource'])->name('editressource');
Route::get('/logout',[LogoutController::class,'logout'])->name('logout');

Auth::routes();
