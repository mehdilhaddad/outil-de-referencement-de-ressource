# Projet : Outils de référencement de ressources


## Installation du projet

1. Cloner le projet:

`git@gitlab.com:mehdilhaddad outil-de-referencement-de-ressource.git`

2. Installation des dépendences dont le projet à besoin :
  
`composer install`

3. Crée le fichier .env :

Copier le fichier .env.example est y introduire les accés à la base données.

`DB_DATABASE=Le nom de votre base donnée.`
`DB_USERNAME=Votre nom d'utilisateur MySQL.`
`DB_PASSWORD=Votre mot de passe MySQL.`

4. Définir de votre clé d'application :

`php artisan key:generate`


## Développement

1. Lancer votre serveur PHP :

`php artisan serve`

2. Lancer la bibliothèque BULMA :

`npm start`

## Documentations

### Php Composer

https://getcomposer.org/download/

### Installation de Laravel

https://laravel.com/docs/8.x/installation#installation-via-composer

### Installation de Bulma et de Sass

https://bulma.io/documentation
