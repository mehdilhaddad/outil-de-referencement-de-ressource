<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use  App\Models\Categorie;
use  App\Models\User;

class Ressource extends Model
{
    use HasFactory;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'synopsis',
        'lien',
        'auteur_id',
        'categorie_id',
    ];
    
    public function categorie() {

        return $this->belongsTo(Categorie::class);
    }
    
    public function user() {
        
        return $this->belongsTo(User::class);

    }
}
