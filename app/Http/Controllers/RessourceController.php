<?php


namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use App\Models\Ressource;
use App\Models\Categorie;


class RessourceController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function PageRessource($id){

        $ressource = Ressource::findOrFail($id);
        
        //return $ressource->name_ressource; 
        
        return view('ressource', ['ressource'=>$ressource]);
    }

    public function DeleteRessource($id) {

        // 1. On charge la ressource depuis la basse de données
        
        $ressource = Ressource::findOrFail($id);

        if (!Auth::check() || Auth::user()->id !== $ressource->auteur_id) {
            
            abort(403);
        }
        
        // 2. On la supprime
        $ressource->delete();
        
        // 3. On redirige l'utilisateur vers la page d'accueil avec un message disant que la ressource
        // a bien été supprimé 

        return redirect('/')->with('success', 'Votre ressource à été supprimé avec succés');
    }

    public function EditRessource($id) {
       
        $ressource = Ressource::findOrFail($id);

        //ici vérifier que l'utilisateur connecté est bien l'auteur de la ressource.
        if (!Auth::check() || Auth::user()->id !== $ressource->auteur_id) {
            
            abort(403);
        }

        $categories = Categorie::all();

        return view('edit', ['ressource'=>$ressource, 'categories' =>$categories]);
    }

    public function UpdateRessource($id) {
        
        $validatedData = request()->validate([
            'name' => ['required','string','min:4','max:255'],
            'lien' => ['required','url'],
            'categorie_id' => ['required'],
            'synopsis' => ['required','string','max:2000'],
        ]);

        Ressource::whereId($id)->update($validatedData);

        return redirect('/')->with('success', 'Votre ressource à été mise à jour avec succèss');
    }
}

