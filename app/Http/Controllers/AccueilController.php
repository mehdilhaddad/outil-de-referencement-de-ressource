<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Ressource;


class AccueilController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function PageAccueil(){
    
        $ressources = Ressource::simplePaginate(30);

        return view('accueil', ['ressources' => $ressources]);

    }
}
