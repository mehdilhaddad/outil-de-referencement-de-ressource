<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Ressource;
use App\Models\Categorie;


class AjoutRessourceController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function PageAjoutdeRessource() {

        $categories = Categorie::all();

        return view('ajoutressource', ['categories' => $categories]);
    }
    

    public function FormRessource(){
        
        // On récupére les données validées du formulaire
        $validatedData = request()->validate([
            'name' => ['required','string','min:4','max:255'],
            'lien' => ['required','url'],
            'categorie_id' => ['required'],
            'synopsis' => ['required','string','max:2000'],
        ]);

        // On ajouter l'id de l'utilisateur qui ajoute la ressource
        $validatedData['auteur_id'] = Auth::id();

        $ressource = Ressource::create($validatedData);

        return redirect('/ressource/' . $ressource->id)->with('success', 'Ressource créée avec succès');
    }

}   





  
            
            
            