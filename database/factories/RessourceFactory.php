<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Categorie;
use App\Models\Ressource;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class RessourceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Ressource::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->sentence(rand(3, 7)),
            'synopsis' => $this->faker->paragraph(rand(5, 25)),
            'lien' => $this->faker->url(),
            'auteur_id' => User::all()->random()->id,
            'categorie_id' => Categorie::all()->random()->id,
        ];
    }
}
