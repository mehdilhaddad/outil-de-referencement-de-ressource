<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CategorieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        
        $categories = [
            'Javascript',
            'PHP',
            'Python',
            'CSS',
            'HTML',
            'Bootstrap',
        ];

        foreach($categories as $name) {
            DB::table('categories')->insert([
                'name' => $name,                
            ]);
        }
    }
}
