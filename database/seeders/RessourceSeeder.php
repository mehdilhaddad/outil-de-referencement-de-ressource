<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\Ressource;


class RessourceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        
        // Ancien code du seeder sans passer par UserFactory
        // $ressource = 0;

        // while($ressource<10) {

        //     DB::table('ressources')->insert([
        //         'name' => Str::random(10),
        //         'synopsis' => Str::random(10),
        //         'lien' => Str::random(10),
        //         'auteur_id' => rand(1, 10),
        //         'categorie_id' => rand(1, 10),
                
        //     ]);

        //     $ressource = $ressource +1;
        // }


        Ressource::factory(50)->create();
        
    }

    
}
