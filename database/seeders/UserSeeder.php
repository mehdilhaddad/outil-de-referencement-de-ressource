<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        
        // Ancien code du seeder sans passer par UserFactory
        // $users = 0;

        // while($users<10) {

        //     DB::table('users')->insert([
        //         'name' => Str::random(10),
        //         'email' => Str::random(10).'@gmail.com',
        //         'password' => Hash::Make('password'),
                
        //     ]);

        //     $users = $users +1;
        // }
        
        User::factory(10)->create();
    }

    
}
