@extends('layouts.app')

@section('title', $ressource->name)

@section('content')
<div class="page-ressource">
    <h1>
        {{$ressource->name}}
    </h1>

    <p class="categorie">Catégorie: {{$ressource->categorie->name}}</p>

    <p class="label">Résumé de la ressource.</p>

    <p class="synopsis">{{$ressource->synopsis}}</p>

    <p class="label">Consulter la documentation de la ressource.</p>
    
    <p class="lien">
        <a href="{{$ressource->lien}}" target="_blank">{{$ressource->lien}}</a>
    </p>

        <!-- Ici je veux ce lien que si :
    1. Je suis connecté et je suis l'auteur de la ressource.
    2. Ou je suis formateur -->
    <div class="position-ressource">
        @auth
        @if (Auth::user()->id === $ressource->auteur_id)
            <p><a 
                class="delete-ressource" 
                onclick="return confirm('Etes-vous sûr de vouloir supprimer cette ressource ?')"
                href="{{ route('deleteressource', ['id' => $ressource->id]) }}">
                Supprimer cette ressource
            </a></p>

            <p class="update-ressource"><a 
                href="{{ route('updateressource', ['id' => $ressource->id]) }}">
                Modifier cette ressource
            </a></p>
        @endif
        @endauth
    </div>    
</div>
@endsection






