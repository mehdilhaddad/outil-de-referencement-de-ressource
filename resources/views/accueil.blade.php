@extends('layouts.app')

@section('title', 'Accueil')

@section('content')
   
<div class="ressources">
@foreach($ressources as $ressource)
    <div class="ressource">
        <h2>
            <a href="/ressource/{{ $ressource->id }}">{{ $ressource->name }}</a>
        </h2>
        <p>{{ Str::limit($ressource->synopsis, 200, $end='...') }}</p>
        <div>
            <a class="btn btn-black" href="/ressource/{{ $ressource->id }}">Voir la ressource</a>
        </div>    
    </div>
@endforeach

{{ $ressources->links() }}

</div>

@endsection