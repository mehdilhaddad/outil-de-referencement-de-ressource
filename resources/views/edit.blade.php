@extends('layouts.app')

@section('content')
    
    <div class="modification">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Modifier cette ressource.') }}</div>
                    <div class="card-body">
                        <form action="{{ route('updateressource', $ressource->id ) }}" method="post">
                            {{ csrf_field() }}
                            
                            <div class="col-md-6">
                                <i class="far fa-newspaper"></i><input type="text" name="name" value="{{$ressource->name}}" placeholder="Nom de la ressource." required>
                            </div>
                            
                            <div class="col-md-6">
                                <i class="fas fa-link"></i><input type="url" name="lien" value="{{$ressource->lien}}" placeholder="Lien de la ressource." required pattern="https://.*" title="L'URL doit obligatoirement contenir un https.">
                            </div>
                            
                            <div>
                                <select name="categorie_id">
                                    <option value="">Sélectionnez une catégorie</option>
                                    @foreach ($categories as $cat)
                                        <option value="{{ $cat->id }}" @if ($cat->id === $ressource->categorie_id) selected="selected" @endif>{{ $cat->name }}</option>
                                    @endforeach
                                </select>
                            </div>
            
                            <div>
                                <textarea rows="2" cols="18" name="synopsis"  placeholder="Synopsis de la ressource." required>{{$ressource->synopsis}}</textarea>
                            </div>

                            <div class="actions">
                                <button type="submit" class="btn-connect">Modifier la ressource</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>     

@endsection
        
 