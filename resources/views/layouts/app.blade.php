<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">            
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>@yield('title') | Outils de référencement de ressources</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="/css/app.css">
    </head>      
    <body>
        <div class="layout">
            <div class="header">
                <div id="rectangle">
                @auth
                    Bonjour {{ Auth::user()->name }}
                @endauth
                </div>

                <nav class="navbar" role="navigation" aria-label="main navigation">
                    <div class="navbar-brand">
                        <a class="navbar-item">
                            <img id="logo" src="/Simplon.jpg" alt="Logo Simplon">
                        </a>
                        <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                            <span aria-hidden="true"></span>

                            <span aria-hidden="true"></span>

                            <span aria-hidden="true"></span>
                        </a>
                    </div>
                    <div id="navbarBasicExample" class="navbar-menu">
                        <div class="navbar-start">
                            @auth
                                <a href="{{ route('ajoutressource') }}" class="navbar-item is-active">
                                    Ajouter une Ressource
                                </a>
                            @endauth
                            <a href="/" class="navbar-item is-active">
                                    Ressources
                            </a>
                            <div class="navbar-item has-dropdown is-hoverable">
                                <a class="navbar-link">
                                    Les Catégories
                                </a>          
                                <div class="navbar-dropdown">
                                    <a href="https://www.php.net/manual/fr/language.functions.php" class="navbar-item">
                                        Les Fonctions
                                    </a>
                                    <hr class="navbar-divider">                
                                    <a href="https://www.php.net/manual/fr/control-structures.if.php" class="navbar-item">
                                        Les Conditions
                                    </a>                
                                    <hr class="navbar-divider">
                                    <a href="https://openclassrooms.com/fr/courses/918836-concevez-votre-site-web-avec-php-et-mysql/912133-les-boucles" class="navbar-item">
                                        Les Boucles
                                    </a>                
                                    <hr class="navbar-divider">
                                    <a href="https://www.php.net/manual/fr/language.types.array.php" class="navbar-item">
                                        Les Tableaux
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="navbar-end">
                        <div class="navbar-item">
                            @guest
                            <a href="{{ route('login') }}" class="navbar-item is-active">
                                Connexion
                            </a>
                            <a href="{{ route('register') }}" class="navbar-item is-active">
                                Créer un compte
                            </a>
                            @else
                            <div class="avatar">                
                                <img id="avatar" src="/avatar profil.png" alt="avatar-profil">            
                                <a href="{{ route('logout') }}" class="navbar-item is-active">
                                    Se déconnecter
                                </a>
                            </div>        
                            @endguest
                        </div>    
                    </div>
                </nav>
            </div>
            <!-- <div class="btn-group">
                <button class="btn">HTML5</button>
                <button class="btn">CSS3</button>
                <button class="btn">PHP</button>
                <button class="btn">JAVASCRIPT</button>
            </div> -->
            <main class="main">
                @include('layouts/messages')
                @yield('content')
            </main>

            <footer class="footer">
                <div class="align">
                    <div id="copyright">
                        <p>© 2021 Simplon.co . All rights reserved</p>
                    </div>
                    <div id="pied-de-page">
                        <a href="/ajouter-une-ressource" class="couleur">
                            Ajouter une Ressource |
                        </a> 
                        <a href="/login" class="couleur">
                            Connexion |
                        </a>
                        <a href="/" class="couleur">
                            Ressource |
                        </a>
                        <a href="#" class="couleur">
                            Les Catégories |
                        </a>
                    </div>
                </div>
            </footer>
        </div>
        <script type="text/javascript" src="/app.js"></script>
    </body>
</html>