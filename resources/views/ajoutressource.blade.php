@extends('layouts.app')

@section('title', 'Ajout de ressource')

@section('content')

    <div class="ajout-ressource">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Ajouter une nouvelle ressource.') }}</div>
                    <hr>

                    <div class="card-body">
                        <form action="{{ route('ajoutressource') }}" method="post">
                            {{ csrf_field() }}

                            <div class="col-md-6">
                                <i class="far fa-newspaper"></i><input type="text" name="name" placeholder="Nom de la ressource." required>
                            </div>

                            <div class="col-md-6">
                                <i class="fas fa-link"></i><input type="url" name="lien" placeholder="Lien de la ressource." required pattern="https://.*" title="L'URL doit obligatoirement contenir un https.">
                            </div>

                            <div>
                                <select name="categorie_id">
                                    <option value="">Sélectionnez une catégorie</option>
                                    @foreach ($categories as $cat)
                                    <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                    @endforeach
                                </select>
                            </div>
            
                            <div>
                                <textarea rows="2" cols="18" name="synopsis" placeholder="Synopsis de la ressource." required></textarea>
                            </div>

                            <div class="actions">
                                <button type="submit" class="btn-connect">Créer la ressource</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>     
@endsection










